Example code illustrating the use of Pi4J code to control a Raspberry Pi camera. 

### Instructions for compiling and running
Pi4J code requires root permission to run; thus, the app must be ran from the command line.

Cd to the project directory and run the following commands:<br>
_To compile_: mvn clean package <br>
_To run_: sudo target/distribution/run.sh 

