
module  com.mycompany.camerapi4jexamplecode {
    requires com.pi4j;
    requires com.pi4j.plugin.pigpio;
    
    requires org.slf4j;
    requires org.slf4j.simple;
    
    requires java.logging;
    
    uses com.pi4j.extension.Extension;
    uses com.pi4j.provider.Provider;
}
